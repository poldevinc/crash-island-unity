﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemHolderManager : MonoBehaviour {


    public GameObject Arm;
    public GameObject axe;

    QuestManager questManager;
    GameInjection gameInjection = new GameInjection();
    ControlManager controlManager = new ControlManager();
    ActionHandler actionHandler = new ActionHandler();


    void Start () {
        actionHandler = gameInjection.getActionHandler();
        questManager = gameInjection.getQuestManager();
        controlManager = gameInjection.getControlManager();
    }


    private void OnTriggerStay(Collider collider)
    {
        if (actionHandler.isPersonNear(collider))
        {
            actionHandler.showActionText(true);
            actionHandler.setActionText("Press (F) To Pickup Item");

            if (Input.GetKey(controlManager.pickUpItem))
            {
                 pickUpItem();
         
            }

        }

    }

    private void OnTriggerExit(Collider other)
    {
        actionHandler.showActionText(false);
    }

    private void pickUpItem()
    {
        Arm.SetActive(true);
        actionHandler.showActionText(false);
        questManager.quest1.isOn = true;
        Destroy(axe);
    }

}
