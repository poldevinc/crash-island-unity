﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlaceholderBuilder : MonoBehaviour {


    public GameObject tent;

    public int buildItemDurationSecond = 10;

    GameInjection gameInjection = new GameInjection();
    ControlManager controlManager = new ControlManager();
    ActionHandler actionHandler = new ActionHandler();

    void Start () {
        controlManager = gameInjection.getControlManager();
        actionHandler = gameInjection.getActionHandler();
    }
	
	// Update is called once per frame
	void Update () {

    }

    private void OnTriggerStay(Collider collider)
    {
        if (actionHandler.isPersonNear(collider))
        {
            actionHandler.showActionText(true);
            actionHandler.setActionText("Press (B) To Build Tent");
            if (Input.GetKey(controlManager.build))
            {
                actionHandler.setSliderText("Building...");
                actionHandler.showActionText(false);
                actionHandler.showSlider(true);
                actionHandler.setSliderValue(Time.deltaTime / buildItemDurationSecond);
                 if(actionHandler.getSliderValue() == 1)
                    {
                    castComplete();
                    }
            }
            else
            {
                actionHandler.clearSlider();
                actionHandler.showSlider(false);
             

            }


        }

    }
    private void OnTriggerExit(Collider other)
    {
        actionHandler.showActionText(false);
        actionHandler.clearSlider();
        actionHandler.showSlider(false);
    }

    private void castComplete()
    {
        actionHandler.showSlider(false);
        tent.SetActive(true);
        Destroy(this.gameObject);
    }
}
