﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnColided : MonoBehaviour {

    public int minRandomNumber = 0;
    public int maxRandomNumber = 10;
    public treeModel treeModel  = new treeModel();
    private weponTreePresenter presenter = new weponTreePresenter();
    public int log = 0;
    public int sticks = 0;
    public int palmLeaf = 0;

 
    private void Start(){
        RandomizeItem();
        presenter.SetOnColided(this);

       
    }

    private void OnTriggerEnter(Collider collider)
    {
        presenter.Tree(collider,this.gameObject);


    }

   private void RandomizeItem()
    {
        if(this.gameObject.tag == "coconut"){
            coconutItems();
            return;
        }
        treeItems();
    }

    private void treeItems()
    {
        treeModel.setRandomLoots(minRandomNumber, maxRandomNumber);
        log = treeModel.normalTreeLog;
        sticks = treeModel.normalStick;
        palmLeaf = 0;
    }

    private void coconutItems()
    {
        treeModel.setRandomLoots(minRandomNumber, maxRandomNumber);
        log = treeModel.normalTreeLog;
        sticks = 0;
        palmLeaf = treeModel.palmTreeLeaf;
    }
}
