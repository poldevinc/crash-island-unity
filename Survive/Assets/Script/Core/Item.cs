﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item  {

    public string name = "";
    public int size = 0;
    public GameObject image ;
    public string typeOfItem = "";
}
