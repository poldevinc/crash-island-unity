﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRLook : MonoBehaviour {

    public Transform vrCamera;
    public float toggleAngle = 30.0f;
    public bool showPanel;
    public GameObject panel;
    private CharacterController cc;
	// Use this for initialization
	void Start () {
        cc = GetComponent<CharacterController>();
        panel = GameObject.Find("Canvas");
        panel.SetActive(false);
        //panel = GetComponent<GameObject>();
    }
	
	// Update is called once per frame
	void Update () {
		if (vrCamera.eulerAngles.x > toggleAngle && vrCamera.eulerAngles.x < 90.0f)
        {
            showPanel = true;
        }
        else
        {
            showPanel = false;
        }
        if (showPanel)
        {
            panel.SetActive(true);
            //Vector3.forward = vrCamera.TransformDirection(Vector3.forward);
            //cc.SimpleMove(forward * speed);
        }
        else
        {
            panel.SetActive(false);
        }
        
	}
}
