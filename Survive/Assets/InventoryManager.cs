﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour {

    GameInjection gameInjection = new GameInjection();
    itemManager itemManager;
    ControlManager controlManager = new ControlManager();
    public GameObject itemSlot;
    public GameObject itemContainer;
    public GameObject inventory;

    public Dictionary<string, Item> itemLists = new Dictionary<string, Item>();
    void Start()
    {
        itemManager = gameInjection.getItemManager();
        controlManager = gameInjection.getControlManager();

  




    }

        // Update is called once per frame
        void Update () {
    
        if (Input.GetKeyDown(KeyCode.R)){
            inventory.SetActive(!inventory.activeSelf);
            if (inventory.activeSelf)
            {
                setItemSlot();
                return;
            }
            Clear(itemContainer.transform);
        }
    }

    

    private void setItemSlot()
    {
        
        itemLists = itemManager.getItemList();
        if (itemContainer.transform.childCount  < itemLists.Count)
        {
            foreach (KeyValuePair<string, Item> itemList in itemLists)
            {
                GameObject go = Instantiate(itemSlot) as GameObject;
                go.transform.parent = itemContainer.transform;
                go.transform.localPosition = Vector3.zero;
                go.transform.localScale = new Vector3(1, 1, 1);
                go.transform.localRotation = Quaternion.Euler(0, 0, 0);

               GameObject counterBG = go.gameObject.transform.GetChild(2).gameObject;
                
                GameObject itemSize = counterBG.gameObject.transform.GetChild(0).gameObject;

                itemSize.GetComponent<Text>().text = itemList.Value.size.ToString();


                GameObject image = go.gameObject.transform.GetChild(1).gameObject;
                //clears the gameobject on prefab
                Destroy(image);

                GameObject imagePrefab = itemList.Value.image;

           

                Transform ItemImage = (Instantiate(itemList.Value.image) as GameObject).transform ;
                ItemImage.parent = go.transform;
                ItemImage.localPosition = imagePrefab.transform.localPosition;
                ItemImage.localRotation = Quaternion.Euler(0, 0, imagePrefab.transform.rotation.eulerAngles.z);
  
                ItemImage.transform.parent = go.transform;

                ItemImage.transform.parent = imagePrefab.transform;

                ItemImage.transform.localScale = new Vector3(1, 1, 1);


                //set the counter to front
                counterBG.transform.SetAsLastSibling();


            }
        }
           
    }

    private void Clear(Transform transform)
    {
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        
    }
}
