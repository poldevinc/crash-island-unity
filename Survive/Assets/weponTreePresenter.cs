﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class weponTreePresenter : MonoBehaviour{

    
    private OnColided treeColided;

    GameInjection gameInjection = new GameInjection();
    itemManager itemManager;
    void Start () {


    }
	
    public void SetOnColided(OnColided colided){
        treeColided = colided;

    }
	// Update is called once per frame
	void Update () {
     
        

    }

    public void Tree(Collider collision, GameObject treeGObj){
        //  Destroy(GetComponent<collision.GetComponentInChildren.GetComponent.to);
        // Destroy(GetComponent<TerrainCollider>());


        GameObject tree = treeGObj.transform.parent.gameObject;
        Debug.Log(tree.name);
        if (tree.name == "Broadleaf_Mobile_With_Collider" )
        {
            RandomVariable();
             if (treeColided.log <= 0 && treeColided.sticks <= 0 && treeColided.palmLeaf <=0)
            {
                Destroy(tree);
              }
        }
        else if (treeGObj.tag == "coconut")
        {
            RandomVariable();
            if (treeColided.log <= 0 && treeColided.sticks <= 0 && treeColided.palmLeaf <= 0)
            {
                Destroy(treeGObj.gameObject);
            }
        }





    }


    public void RandomVariable(){
        int random = Random.Range(0, 3);
  
        itemManager = gameInjection.getItemManager();

        Debug.Log(random);
        if (random == 0){
            if (treeColided.log > 0){
                treeColided.log--;
                itemManager.addItem(itemManager.log, itemManager.logImage);
            }
        }
        else if (random == 1){
            if(treeColided.sticks > 0)
            {
                treeColided.sticks--;
                itemManager.addItem(itemManager.stick, itemManager.stickImage);
            }
               
        }
        else if (random == 2){
            if (treeColided.palmLeaf > 0)
            {
                treeColided.palmLeaf--;
                itemManager.addItem(itemManager.leaf, itemManager.leafImage);
            }
             

        }
    }
}
