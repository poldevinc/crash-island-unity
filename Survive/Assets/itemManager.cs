﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemManager : MonoBehaviour {

    public GameObject logImage;
    public GameObject stickImage;
    public GameObject leafImage;

    public Dictionary<string, Item> itemList = new Dictionary<string, Item>();

    GameInjection gameInjection = new GameInjection();
    QuestManager questManager;
    // Use this for initialization

    public static string log = "logs";
    public static string leaf = "leaf";
    public static string stick = "stick";
    void Start () {


        questManager = gameInjection.getQuestManager();

    }

    private int getItemSize(Item item , string itemName)
    {
        if (!itemList.ContainsKey(itemName))
        {
            item.size = item.size + 1;
            return item.size;
        }
        item.size = itemList[itemName].size++;
        return item.size;
    }


    public void addItem(string ItemName, GameObject ItemImage)
    {
        Item item = new Item();
        item.size = getItemSize(item, ItemName);
        item.name = ItemName;
        item.image = ItemImage;

        itemList.Add(item.name, item);
    }

    public int getLogSize()
    {
        return checkIfItemExist(log);
    }

    public int getStickSize()
    {
        return checkIfItemExist(stick);
    }

    public int getLeafSize()
    {
        return checkIfItemExist(leaf);
    }

    private int checkIfItemExist(string key)
    {
        int size = 0;
        if (itemList.ContainsKey(key))
        {
            size = itemList[key].size;
        }
        return size;
    }


    public Dictionary<string, Item> getItemList()
    {
        return itemList;
    }

}
