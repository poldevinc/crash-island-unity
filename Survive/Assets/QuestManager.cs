﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestManager : MonoBehaviour {

    // Use this for initialization
    public GameObject questUI;
    public Toggle quest1;
    public Toggle quest2;
    public Toggle logs;
    public Toggle stick;
    public Toggle leaf;


    public Text logText;
    public Text stickText;
    public Text leafText;

    int maxLog = 4;
    int maxLeaf = 20;
    int maxStick = 10;

    public GameObject tent;

    GameInjection gameInjection = new GameInjection();
    itemManager itemManager;
    ControlManager controlManager = new ControlManager();
    Dictionary<string, Item> itemList = new Dictionary<string, Item>();

    private int logSize;
    private int stickSize;
    private int leafSize;
    void Start() {


        itemManager = gameInjection.getItemManager();
        controlManager = gameInjection.getControlManager();
    }

    // Update is called once per frame
    void Update() {
        updateObjects();
        tabPressed();
        showTent();

    }

    private void updateObjects()
    {
         logSize = itemManager.getLogSize();
         stickSize = itemManager.getStickSize();
         leafSize = itemManager.getLeafSize();
    }

    public void tabPressed()
    {

            if (Input.GetKeyDown(controlManager.showQuest))
        {

            questUI.SetActive(!questUI.activeSelf);
            if (questUI.activeSelf)
            {
                mission1();
             
            }

        }
    }


    private void mission1()
    {
        

        logText.text = "Log [" + logSize + "/" + maxLog + "]";
        if (logSize >= maxLog)
        {
            logs.isOn = true;
        }

        stickText.text = "Stick [" + stickSize + "/" + maxStick + "]";

        if (stickSize >= maxStick)
        {
            stick.isOn = true;
        }

        leafText.text = "Palm Leave [" + leafSize + "/" + maxLeaf + "]";

        if (leafSize >= maxLeaf)
        {
            leaf.isOn = true;
        }

    }

    private void showTent()
    {
        if (logSize >= maxLog && leafSize >= maxLeaf && stickSize >= maxStick)
        {

            tent.SetActive(true);

        }
    }
}
