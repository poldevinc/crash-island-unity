﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlManager : MonoBehaviour {

    public int attack;
    public KeyCode walkForward;
    public KeyCode walkBackWard;
    public KeyCode walkRight;
    public KeyCode walkLeft;
    public KeyCode showQuest;
    public KeyCode walkJump;
    public KeyCode pickUpItem;
    public KeyCode openInventory;
    public KeyCode build;



}
