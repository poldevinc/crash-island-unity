﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DayQuestManager : MonoBehaviour {

    // Use this for initialization
    public Text Day;
    private int DayNumber;
    public float fadeOutTime =1;
    public Image QuestScroll;
    public bool isDayfinnihed;
    bool isfinished = false;
    void Start () {
        StartCoroutine(FadeImage(true,Day,null));

    }
	
	// Update is called once per frame
	void Update () {
   
        if (isfinished == true)
        {
           
                QuestScroll.gameObject.SetActive(true);

            isfinished = false;
        }

    }


    IEnumerator FadeImage(bool fadeAway,Text text,Image image)
    {
        // fade from opaque to transparent
        if (fadeAway)
        {
            // loop over 1 second backwards
            for (float i = 1; i >= 0; i -= Time.deltaTime)
            {
                // set color with i as alpha
                if(text != null){
                    text.color = new Color(1, 1, 1, i);
                }

                if (image != null)
                {
                    image.color = new Color(1, 1, 1, i);
                }

              
                yield return null;
            }
            text.gameObject.SetActive(false);
            isfinished = true;
        }
        // fade from transparent to opaque
        else
        {
            // loop over 1 second
            for (float i = 0; i <= 1; i += Time.deltaTime)
            {
                // set color with i as alpha
                if (text != null)
                {
                    text.color = new Color(1, 1, 1, i);
                }

                if (image != null)
                {
                    image.color = new Color(1, 1, 1, i);
                }

                yield return null;
            }
        }
    }
}
