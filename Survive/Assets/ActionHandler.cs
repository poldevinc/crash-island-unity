﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionHandler : MonoBehaviour {

   
    public Text ActionText;
    public Slider slider;
    public Text sliderText;

    public bool isPersonNear(Collider other)
    {
        if (other.gameObject.name == "FPSController")
        {
            return true;
        }
        return false;
    }

    public void showActionText(bool value)
    {
        ActionText.gameObject.SetActive(value);
    }

    public void setActionText(string text)
    {
        ActionText.text = text;
    }

    public void setSliderValue(float value)
    {
        slider.value += value;
    }

    public void clearSlider()
    {
        slider.value = 0;
    }

    public void setSliderText(string text)
    {
        ActionText.text = text;
    }
    public void showSlider(bool value)
    {
        slider.gameObject.SetActive(value);
    }

    public float getSliderValue()
    {
        return slider.value;
    }

}
