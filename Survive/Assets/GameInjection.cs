﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInjection {

 
   public QuestManager getQuestManager()
    {
        GameObject go = GameObject.Find("DayQuestManager");
        QuestManager questManager = (QuestManager)go.GetComponent(typeof(QuestManager));

        return questManager;
    }
       
    public itemManager getItemManager()
    {
        GameObject go = GameObject.Find("PlayerManager");
        itemManager itemManager = (itemManager)go.GetComponent(typeof(itemManager));
        return itemManager;
    }


    public ControlManager getControlManager()
    {
        GameObject go = GameObject.Find("ControllManager");
        ControlManager controlManager = (ControlManager)go.GetComponent(typeof(ControlManager));

        return controlManager;
    }


    public ActionHandler getActionHandler()
    {
        GameObject go = GameObject.Find("PlayerManager");
        ActionHandler actionHandler = (ActionHandler)go.GetComponent(typeof(ActionHandler));

        return actionHandler;
    }
}
