﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class playerManager : MonoBehaviour {
    public Animator anim;
    public Collider axeHitBox;
    // Use this for initialization

    public int logs;
    public int sticks;
    public int leaf;

 
  
    private weponTreePresenter wepon;

    GameInjection gameInjection = new GameInjection();
    ControlManager controlManager = new ControlManager();
    void Start()
    {
        controlManager = gameInjection.getControlManager();
    }

    // Update is called once per frame
    void Update () {
     
        if (Input.GetMouseButtonDown(controlManager.attack))
        {
            anim.SetBool("Attack", true);
            axeHitBox.isTrigger = true;

        }
        else if (Input.GetMouseButtonUp(controlManager.attack))
        {
            anim.SetBool("Attack", false);
            axeHitBox.isTrigger = false;
        }
       
    }

 
}
